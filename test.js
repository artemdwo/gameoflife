const engine = require('./index');

test('engine exist', () => {
    expect(typeof engine).toEqual('function');
  });

test('grid with no life', () => {
    let grid_in, grid_out;
    grid_in = grid_out = [[0,0,0,0,0],
                        [0,0,0,0,0],
                        [0,0,0,0,0],
                        [0,0,0,0,0],
                        [0,0,0,0,0] ];
    expect(engine(grid_in)).toEqual(grid_out);
});

test('grid has a single live cell', () => {
    let grid_in = [[0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,1,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0] ];

    let grid_out = [[0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0] ];
    
    expect(engine(grid_in)).toEqual(grid_out);
});

test('grid has two live cells [NOT comrades]', () => {
    let grid_in = [[1,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,1]];

    let grid_out = [[0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0] ];
    
    expect(engine(grid_in)).toEqual(grid_out);
});

test('grid has 3 cells: two comrades + 1 stranger', () => {
    let grid_in = [[0,0,0,0,0],
                [1,1,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,1] ];

    let grid_out = [[0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0] ];

    expect(engine(grid_in)).toEqual(grid_out);
});

test('grid has 3 live cells (comrades)', () => {
    let grid_in = [[0,0,0,0,0],
                [1,1,1,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0] ];

    let grid_out = [[0,1,0,0,0],
                [0,1,0,0,0],
                [0,1,0,0,0],
                [0,0,0,0,0],
                [0,0,0,0,0] ];

    expect(engine(grid_in)).toEqual(grid_out);
});