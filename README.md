# Conway's Game of Life

Details: https://en.wikipedia.org/wiki/Conway's_Game_of_Life#Examples_of_patterns

## Run

`node index.js`

## Test

With Jest installed globally

`jest test.js`

or 

`npm test`

With Jest installed via `npm i`

`npx jest test.js`

Run tests continuously

`npm run tw`